<?php
namespace MMC\Sitesetup;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;


class Utility {

  static $imageService = NULL;
  static $objectManager = NULL;

  protected static function getObjectManager(){
    return self::$objectManager ?? self::$objectManager = GeneralUtility::makeInstance(ObjectManager::class);
  }

  protected static function getImageService(){
    return self::$imageService ?? self::getObjectManager()->get(ImageService::class);
  }

  /**
  * renderResponsiveImageSet
  *
  * Tries to render 1x, 2x and 3x variants for responsive image display of the original file.
  * Applies image cropping, resizes image to maxWidth / maxHeight (values for 1x), tries to
  * render 2x and 3x if the original images resolution allows that
  *
  * @var \TYPO3\CMS\Core\Resource\FileInterface $originalImage
  * @var integer $maxWidth maxwidth of processed 1x image
  * @var integer $maxHeight maxheight of processed 1x image
  * @return array<\TYPO3\CMS\Core\Resource\FileInterface> [0]=> 1x image, [1]=> 2x image, [2] => 3x image
  */
  public static function renderResponsiveImageSet( $originalImage, $maxWidth, $maxHeight ){
    $imageService = self::getImageService();
    // fetch crop area
    $cropString = $originalImage->getProperty('crop');
    $cropVariantCollection = CropVariantCollection::create((string)$cropString);
    $cropArea = $cropVariantCollection->getCropArea('default');
    $absoluteCropArea = $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image);
    // process 1x image
    $processedImages[0] = $imageService->applyProcessingInstructions( $originalImage,
      [ 'maxWidth' => $maxWidth, 'maxHeight' => $maxHeight, 'crop' => $absoluteCropArea ]
    );
    // get width of processed file and (cropped?) original resource
    $processed1xImageWidth = $processedImages[0]->getProperty('width');
    $maxOriginalImageWidth = $absoluteCropArea ? $absoluteCropArea->asArray()['width'] : $originalImage->getProperty('width');
    // try to process 2x image
    if( $maxOriginalImageWidth >= ($w = $processed1xImageWidth * 2 ) ){
      $processedImages[1] = $imageService->applyProcessingInstructions( $originalImage,
        [ 'width' => $w, 'crop' => $absoluteCropArea ]
      );
    }
    // try to process 3x image
    if( $maxOriginalImageWidth >= ($w = $processed1xImageWidth * 3 ) ){
      $processedImages[2] = $imageService->applyProcessingInstructions( $originalImage,
        [ 'width' => $w, 'crop' => $absoluteCropArea ]
      );
    }
    return $processedImages;
  }

  /**
  * setSessionVar
  * set a session variable in the frontend user session in TypoScript using USER_INT cObject
  * $conf['key']: key, $conf['value']: value to store
  */
  public static function setSessionVar(string $content, array $conf): string {
    $GLOBALS['TSFE']->fe_user->setAndSaveSessionData($conf['key'], $conf['value']);
    return '';
  }

}
