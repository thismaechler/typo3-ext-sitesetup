<?php
namespace MMC\Sitesetup\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Exception;


class ResponsiveImageViewHelper extends AbstractTagBasedViewHelper {

  /**
  * @var string
  */
  protected $tagName = 'img';


  /**
  * Initialize arguments.
  */
  public function initializeArguments()
  {
    parent::initializeArguments();
    $this->registerUniversalTagAttributes();
    $this->registerTagAttribute('alt', 'string', 'Specifies an alternate text for an image', false);
    $this->registerArgument('image', 'object', 'a FAL object');
    $this->registerArgument('maxWidth', 'int', 'maximum width of the image');
    $this->registerArgument('maxHeight', 'int', 'maximum height of the image');
    $this->registerArgument('sizes', 'string', 'sizes attribute');
    $this->registerArgument('addHiresImageSource', 'boolean', 'true if a hires-image-source should be added');
  }

  /**
  * Builds a respnsive img srcset with hi-res images if available
  *
  * @throws Exception
  * @return string Rendered tag
  */
  public function render() {
    if ( $this->arguments['image'] === null) {
      throw new Exception('You must specify a File object.');
    }
    $image = $this->arguments['image'];
    $imageSet = \MMC\Sitesetup\Utility::renderResponsiveImageSet( $image,
      $this->arguments['maxWidth'] ?? 0,
      $this->arguments['maxHeight'] ?? 0
    );
    // default image source 1x
    $processedImageWidth = $imageSet[0]->getProperty('width');
    $processedImageHeight = $imageSet[0]->getProperty('height');
    $this->tag->addAttribute('width', $processedImageWidth);
    $this->tag->addAttribute('height', $processedImageHeight);
    $this->tag->addAttribute('src', $imageSet[0]->getPublicUrl() );
    // add srcset and sizes tag if multiple image resolutios are available
    if( count($imageSet) > 1 ){
      $this->tag->addAttribute('srcset', $this->getSrcsetString($imageSet) );
      $this->tag->addAttribute('sizes',
        empty($this->arguments['sizes']) ?
          "(min-width: {$processedImageWidth}px) {$processedImageWidth}px, 100vw" : // default sizes
          $this->arguments['sizes'] // set custom sizes argument
      );
    }
    // set data attribute with the highest available resolution image source
    if( $this->arguments['addHiresImageSource'] ){
      $this->tag->addAttribute('data-hires-src', end($imageSet)->getPublicUrl() );
    }
    // set title tag
    $title = $image->getProperty('title');
    if (empty($this->arguments['title']) && $title) {
      $this->tag->addAttribute('title', $title);
    }
    // The alt-attribute is mandatory to have valid html-code
    $this->tag->addAttribute('alt', '');
    return $this->tag->render();
  }

  /**
  * Builds a respnsive img srcset with hi-res images if available
  *
  * @param array<\TYPO3\CMS\Core\Resource\FileInterface> $imageSet
  * @return string srcset attribute content
  */
  private function getSrcsetString($imageSet){
    $srcsetParts = [];
    foreach($imageSet as $image){
      $srcsetParts[] = $image->getPublicUrl().' '.$image->getProperty('width').'w';
    }
    return implode(', ',$srcsetParts);
  }

}
