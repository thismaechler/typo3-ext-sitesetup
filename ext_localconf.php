<?php
defined('TYPO3_MODE') || die();

(function () {

	// backend user config
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
		'<INCLUDE_TYPOSCRIPT: source="FILE:EXT:sitesetup/Configuration/TypoScript/be_user.typoscript">'
	);

	// rte config
	$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['default'] = 'EXT:sitesetup/Configuration/RTE/ckeditor.yaml';

})();
