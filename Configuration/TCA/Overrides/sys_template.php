<?php
defined('TYPO3_MODE') || die();

(function () {

	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('sitesetup', 'Configuration/TypoScript', 'TYPO3 site setup');

})();
