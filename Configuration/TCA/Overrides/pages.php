<?php

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
	'sitesetup',
	'Configuration/TypoScript/page.typoscript',
	'TYPO3 site setup - page config'
);
