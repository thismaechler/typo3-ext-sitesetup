<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'TYPO3 site setup',
	'description' => 'TYPO3 basic site setup',
	'category' => 'distribution',
	'author' => 'Thĩs Mächler',
	'author_email' => 'maechler@mm-computing.ch',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => '0',
	'clearCacheOnLoad' => 0,
	'version' => '1.0.1',
	'constraints' => array(
		'depends' => array(
			'typo3' => '9.5.0-10.99.99'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
